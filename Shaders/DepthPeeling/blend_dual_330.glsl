#version 330

uniform sampler2D BlendTex;


in vec2 vTexCoord;
out vec4 oColor;

void main()
{
	oColor = texture(BlendTex, vTexCoord);
	// for occlusion query
	if (oColor.a == 0) discard;
}